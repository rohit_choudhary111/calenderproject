import { EventsComponent } from "./events/events.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddeventComponent } from "./addevent/addevent.component";
import { AuthComponent } from "./Auth/auth/auth.component";

const routes: Routes = [
    { path: "", component: EventsComponent },
    { path: "addevent", component: AddeventComponent },
    { path: "login", component: AuthComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
