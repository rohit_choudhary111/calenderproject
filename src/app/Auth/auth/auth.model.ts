export class User {
    constructor(
        public id: string | null,
        public fname: string | null,
        public lname: string | null,
        public email: string | null,
        public token: string | null
    ) {}

    get _token() {
        if (this.token == null) {
            return null;
        }
        return this.token;
    }

    getdata() {
        return this.id, this.fname, this.lname, this.email, this.token;
    }
}
