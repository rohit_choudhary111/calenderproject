import { NgForm } from "@angular/forms";
import { Component } from "@angular/core";
import { AuthService } from "./auth.service";
import { User } from "./auth.model";

@Component({
    selector: "app-auth",
    templateUrl: "./auth.component.html",
    styleUrls: ["./auth.component.css"],
})
export class AuthComponent {
    error: any;
    storedata: any;
    users: any;
    userdata: any;
    isloginMode = true;
    constructor(private authlogin: AuthService) {}
    onswitchMode() {
        this.isloginMode = !this.isloginMode;
    }

    onsubmit(loginform: NgForm) {
        this.authlogin.login(loginform.value).subscribe(
            (result) => {
                this.users = result;
                this.userdata = new User(
                    this.users.user.id,
                    this.users.user.fname,
                    this.users.user.lname,
                    this.users.user.email,
                    this.users.token
                );
                this.error = "";
                this.authlogin.getAuthUser(this.userdata);
            },
            (error) => {
                this.error = error.text || error.message;
            }
        );
        loginform.reset();
    }
}
