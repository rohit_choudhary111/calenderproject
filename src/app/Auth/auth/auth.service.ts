import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class AuthService {
    AuthLoginUser: any;
    constructor(private http: HttpClient) {}
    login(data: any) {
        return this.http
            .post("http://127.0.0.1:8000/api/UserLogin", data)
            .pipe(catchError(this.errorHandle));
    }
    errorHandle(error: HttpErrorResponse) {
        return throwError(error.error || "server error");
    }
    getAuthUser(data: any) {
        console.log(data);
        this.AuthLoginUser = data;
        localStorage.setItem("id", this.AuthLoginUser.id);
        localStorage.setItem("fname", this.AuthLoginUser.fname);
        localStorage.setItem("lname", this.AuthLoginUser.lname);
        localStorage.setItem("email", this.AuthLoginUser.email);
        localStorage.setItem("token", this.AuthLoginUser.token);
    }
}
