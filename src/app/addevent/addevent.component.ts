import { PassdataService } from './../passdata.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-addevent',
  templateUrl: './addevent.component.html',
  styleUrls: ['./addevent.component.css'],
})
export class AddeventComponent implements OnInit {
  constructor(private passdata: PassdataService) {}

  message: any;
  ngOnInit(): void {}
  onSubmit(data: NgForm) {
    this.passdata.posteventdata(data).subscribe((result) => {
      this.message = 'data submitted successfully';
    });
  }
}
