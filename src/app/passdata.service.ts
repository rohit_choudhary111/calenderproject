import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PassdataService {
  eventss: any;
  constructor(private http: HttpClient) {}
  geteventsdata() {
    return this.http.get('http://127.0.0.1:8000/api/event');
  }

  posteventdata(event: any) {
    return this.http.post('http://127.0.0.1:8000/api/event/', event);
  }
}
