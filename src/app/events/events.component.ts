import { PassdataService } from "./../passdata.service";
import { CalendarOptions } from "@fullcalendar/angular";
import { Component, OnInit } from "@angular/core";
import { AuthService } from "../Auth/auth/auth.service";
import { User } from "../Auth/auth/auth.model";
@Component({
    selector: "app-events",
    templateUrl: "./events.component.html",
    styleUrls: ["./events.component.css"],
})
export class EventsComponent implements OnInit {
    event: any[] = [];
    eventdata: any = "";
    data: any;
    constructor(
        private servicedata: PassdataService,
        private getservicedata: AuthService
    ) {
        this.servicedata.geteventsdata().subscribe((result) => {
            this.eventdata = result;
            console.log(this.eventdata);
            console.log(localStorage);
            console.log(this.getservicedata.AuthLoginUser);
        });
    }

    calendarOptions: CalendarOptions = {
        initialView: "dayGridMonth",
        dateClick: this.handleDateClick.bind(this), // bind is important!
        events: this.event,
    };

    handleDateClick(arg: any) {
        console.log(this.eventdata);
        this.eventdata.forEach((element: any) => {
            if (element.date == arg.dateStr) {
                alert(
                    "Event description -" +
                        element.description +
                        " " +
                        "event start from " +
                        element.date +
                        " " +
                        "event end at " +
                        element.enddate
                );
            }
        });
    }
    ngOnInit(): void {
        this.data = new User(
            localStorage.getItem("id"),
            localStorage.getItem("fname"),
            localStorage.getItem("lname"),
            localStorage.getItem("email"),
            localStorage.getItem("token")
        );
        console.log(this.data);
    }
}
